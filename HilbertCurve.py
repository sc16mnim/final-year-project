## Rotate quadrant

def rot ( N, x, y, rx, ry ):
# Introduce rx and ry for boolean control
  if ( ry == 0 ):

#  Reflect.
    if ( rx == 1 ):
      x = N - 1 - x
      y = N - 1 - y

#  Flip.
    t = x
    x = y
    y = t

  return x, y

## XY2D converts a 2D Cartesian coordinate to a 1D Hilbert coordinate.

def xy2d ( x, y ):

  xcopy = x
  ycopy = y

  d = 0

# Divide the square into four quadrants
  s = ( N // 2 )

  while ( 0 < s ):

    if ( 0 <  ( ( xcopy ) & s ) ):
      rx = 1
    else:
      rx = 0

    if ( 0 < ( ( ycopy ) & s ) ):
      ry = 1
    else:
      ry = 0

    d = d + s * s * ( ( 3 * rx ) ^ ry )

# Rotate the first (top left) and last (bottom left) quadrant

    xcopy, ycopy = rot ( s, xcopy, ycopy, rx, ry )

    s = ( s // 2 )

  return d

#
#---------------------------------------------------------------------
#

print("Provide input for the exponent of base 2 (2**m)")

input_text = input ("m: ")
m = int(input_text)
N = 2**m

print("Problem size: " + str(N) + " x " + str(N))

# Print the order of the path in a given problem size N

print ( '        ', end = '' )
print ( '' )
for x in range ( 0, N, 1 ):
  for y in range ( 0, N ):
    d = xy2d ( x, y )
    print ( '%3d' % ( d ), end = '' )
  print ( '' )
