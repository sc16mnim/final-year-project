# Display data of size N by N in 2D format

def displaydata( data, N ):

	for i in range(N):
		for j in range(N):
			print( " {:>2} ".format(data[i][j]), end='' )
		print( '' )

print("Provide input for the exponent of base 2 (2**m)")

input_text = input ("m: ")
m = int(input_text)
N = 2**m
d = 0

print("Problem size: " + str(N) + " x " + str(N))
print()

data = []
for i in range(N):
	data.append( [0.0]*(N) )

for x in range(N):
    for y in range(N):
        d += 1
        data[x][y] = d

# Display the order of the path in a given problem size N
displaydata( data, N )

print()
