import time

## Rotate quadrant

def rot ( N, x, y, rx, ry ):
# Introduce rx and ry for boolean control
  if ( ry == 0 ):

#  Reflect.
    if ( rx == 1 ):
      x = N - 1 - x
      y = N - 1 - y

#  Flip.
    t = x
    x = y
    y = t

  return x, y

## XY2D converts a 2D Cartesian coordinate to a 1D Hilbert coordinate.

def xy2d ( x, y ):

  xcopy = x
  ycopy = y

  d = 0

# Divide the square into four quadrants
  s = ( (N+2) // 2 )

  while ( 0 < s ):

    if ( 0 <  ( ( xcopy ) & s ) ):
      rx = 1
    else:
      rx = 0

    if ( 0 < ( ( ycopy ) & s ) ):
      ry = 1
    else:
      ry = 0

    d = d + s * s * ( ( 3 * rx ) ^ ry )

# Rotate the first (top left) and last (bottom left) quadrant

    xcopy, ycopy = rot ( s, xcopy, ycopy, rx, ry )

    s = ( s // 2 )

  return d

#
#---------------------------------------------------------------------
#

# Function to display the current data of size N by N. Also displays the boundary nodes,
# which are delineated with dashed lines.

def displaydata( data, N ):

	# Display doesn't work for very large meshes.
	if N>25:
		print( "Not displaying data; too large." )
		return

	for x in range(N+2):
		for y in range(N+2):
			print( " {:.6f} ".format(data[x][y]), end="" )

			# Display vertical line to delineate the boundary nodes.
			if y==0 or y==N:
				print( "|", end="" )

		# End the current line
		print()

		# Add line of dashes to delineate boundary values.
		if x==0 or x==N:
			print( "-"*(10*(N+2)+2) )

print("Provide input for the exponent of base 2 (2**m)")

input_text = input ("m: ")
m = int(input_text)
N = 2**m- 2

print("Problem size: " + str(N+2) + " x " + str(N+2))

# Use an array to store the data.
data = []
for x in range(N+2):
	data.append( [0.0]*(N+2) )

# Put some initial values in the data cells(leave the boundary values at zero).
for x in range(1,N+1):
	for y in range(1,N+1):
		data[x][y] = 1.0

# Display the initial data.
print( "Initial data:" )
displaydata( data, N )

print()
#print(data)

#
# Main solver routine -------------------------------------------------
#

# Convert data from 2D to 1D
data1D = [0.0] * (N+2) * (N+2)
for x in range(1,N+1):
    for y in range(1,N+1):
        data1D[xy2d(x,y)] = data[x][y]

# Pre-calcuate the formula. This uses the Gauss-Seidel method.
indexMap = []
for x in range(N+2):
	indexMap.append( [] )
	for y in range(N+2):
		indexMap[-1].append( [0,0,0,0,0] )

for x in range(1,N+1):
    for y in range(1,N+1):
        indexMap[x][y][0] = xy2d(x  ,y  )
        indexMap[x][y][1] = xy2d(x-1,y  )
        indexMap[x][y][2] = xy2d(x+1,y  )
        indexMap[x][y][3] = xy2d(x  ,y-1)
        indexMap[x][y][4] = xy2d(x  ,y+1)

start = time.time()

numIterations = 15			# Can increase this value to get better statistics, i.e. more averaging.

# Updates the data cells
for iteration in range(numIterations):
	for x in range(1,N+1):
		for y in range(1,N+1):
			indices = indexMap[x][y]
			data1D[indices[0]] = 0.25 * ( data1D[indices[1]] + data1D[indices[2]] + data1D[indices[3]] + data1D[indices[4]] )

end = time.time()

#
#---------------------------------------------------------------------
#

# Convert data from 1D to 2D
for x in range(1,N+1):
    for y in range(1,N+1):
        data[x][y]=data1D[xy2d(x,y)]

#
# Display the final data. The values in the middle should now be smaller,
# especially near the edges and corners.

print( "\nFinal data:" )
displaydata( data, N )

print()
print("Execution time: ",end - start)
