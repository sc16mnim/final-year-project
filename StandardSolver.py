import time

#
# Function to display the current data data. Also displays the boundary nodes,
# which are delineated with dashed lines.
#
def displaydata( data, N ):

	# Display doesn't work for very large meshes.
	if N>25:
		print( "Not displaying data; too large." )
		return

	for i in range(N+2):
		for j in range(N+2):
			print( " {:.6f} ".format(data[i][j]), end="" )

			# Display vertical line to delineate the boundary nodes.
			if j==0 or j==N:
				print( "|", end="" )

		# End the current line
		print()

		# Add line of dashes to delineate boundary values.
		if i==0 or i==N:
			print( "-"*(10*(N+2)+2) )

print("Provide input for the exponent of base 2 (2**m)")

input_text = input ("m: ")
m = int(input_text)
N = 2**m- 2

print("Problem size: " + str(N+2) + " x " + str(N+2))

# Use an array to store the data.
data = []
for i in range(N+2):
	data.append( [0.0]*(N+2) )

# Put some initial values in the data cells (leave the boundary values at zero).
for i in range(1,N+1):
	for j in range(1,N+1):
		data[i][j] = 1.0

# Display the initial data.
print( "Initial data:" )
displaydata( data, N )

print()

#
# Main solver routine -------------------------------------------------
#

start = time.time()

numIterations = 15			# Can increase this value to get better statistics, i.e. more averaging.
for iteration in range(numIterations):

	# Perform a single update of the data using Gauss-Seidel. Boundary node remains unchanges.
	for i in range(1,N+1):
		for j in range(1,N+1):
			data[i][j] = 0.25*( data[i-1][j] + data[i+1][j] + data[i][j-1] + data[i][j+1] )

end = time.time()

#
#---------------------------------------------------------------------
#

# Display the final data. The values in the middle should now be smaller,
# especially near the edges and corners.

print( "\nFinal data:" )
displaydata( data, N )

print()
print("Execution time: ",end - start)
